//Slot

/*
 * Class used to store the different operations that the slots will contain.
 */
class Slot { 
  
  int col;
  int type;
  ArrayList params;
  
  Slot (int c,int t,ArrayList p) {  
    col = c;
    type = t;
    params = p;
  } 
  
  void trigger() { 
    switch (type){
      //all
      case HEAD_ALL:  
      transferAllChannelsColor(col);  
      break;
      //col
      case HEAD_COL:
      transferColColor((Integer)params.get(0),col);
      break;
      //row
      case HEAD_ROW:  
      transferRowColor((Integer)params.get(0),col);
      break;
      //diag1
      case HEAD_DIA1:  
      transferDia1Color((Integer)params.get(0),col);
      break;
      //diag2
      case HEAD_DIA2:  
      transferDia2Color((Integer)params.get(0),col);
      break;
      //matrix
      case HEAD_MATRIX: 
      for (int i=0;i<nCols*nRows;i++){
        matrix[i] = (Integer)params.get(i);
      }
      transferMatrix(); 
      break;
    }
  } 
}
