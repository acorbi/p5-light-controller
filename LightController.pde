import oscP5.*;
import netP5.*;
import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import processing.serial.*;
import controlP5.*;

/*******
 DEBUG
 ********/
boolean DEBUG = true;

//if set to true, stores the configuration every time a component is being set
final boolean creatingMode = true;

//deactivates calls to Serial port
final boolean offline = false;

//Signals to control the state machine in the Arduino part
final int HEAD_R = 0x55;
final int HEAD_G = 0x56;
final int HEAD_B = 0x57;
final int HEAD_ROW = 0x58;
final int HEAD_COL = 0x59;
final int HEAD_ALL = 0x60;
final int HEAD_DIA1 = 0x61;
final int HEAD_DIA2 = 0x62;
final int HEAD_MATRIX = 0x63;

//Sketch dimensions
final int H = 700;
final int W = 600;

//Number of rows/columns
final int nRows = 4;
final int nCols = 4;

//Slot and Racks instances
final int nSlots = 16;
final int nRacks= 8;
final int slotPadd = 10;
final int slotHeight = 40;
final int slotWidth = ((W-(slotPadd*nSlots))  / nSlots);
int currentSlot = 0;
int activeSlot = 0;
int currentRack = 0;
int activeRack = 0;
Slot[][] slots = new Slot[nRacks][nSlots];
boolean[] activeRacks = new boolean[nRacks];

//Single items
int[] matrix = new int[nRows*nCols];
Button[][] matrixPads = new Button[nRows][nCols];
PadListener padListener = new PadListener();
KnobListener knobListener = new KnobListener();

//Pitch 
boolean playing = false;
boolean audioSync = false;
boolean audioApply = false;
boolean looping = false;
boolean stealth = false;
final int framerate = 60;
int pitch = 180; 
int beatCounter = 0;

//Array to handle simultaneous keypress events
boolean[] keys = new boolean[526];

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port

//background
PImage bg;

//Audio instances
Minim minim;    //Minim instance
BeatDetect beat; //Beatdetection algorithms
AudioInput in;  //Input from mic
FFT fft;        //Fast furier transform

ControlP5 controlP5;

// COLOR SETTER AREA Knobs
Knob knobR;
Knob knobG;
Knob knobB;
Textlabel lblKnobR;
Textlabel lblKnobG;
Textlabel lblKnobB;
Button resetR_btn;
Button resetG_btn;
Button resetB_btn;

// ALL Triggers
int allTrigger0_col = 0;
int allTrigger1_col = 0;
int allTrigger2_col = 0;
int allTrigger3_col = 0;
Button allTrigger0_btn;
Button allTrigger1_btn;
Button allTrigger2_btn;
Button allTrigger3_btn;


// ROW/COL AREA Knobs
Knob knobRow;
Knob knobCol;
Button knobRow_btn;
Button knobCol_btn;
int row = 0;
int col = 0;
Textlabel knobRow_lbl;
Textlabel knobCol_lbl;
int knobRow_col = 0;
int knobCol_col = 0;

// DIAGONAL AREA Knobs
Knob knobDia1;
Knob knobDia2;
Button knobDia1_btn;
Button knobDia2_btn;
int dia1_col = 0;
int dia2_col = 0;
Textlabel knobDia1_lbl;
Textlabel knobDia2_lbl;
int dia1 = 0;
int dia2 = 0;

Button playBtn;
Button stopBtn;
Button audioSyncBtn;
Button loopBtn;
Button activateRackBtn;
Button resetItemsBtn;
Button stealthModeBtn;
Button saveBtn;
Button loadBtn;

int low_col = 0;
int high_col = 0;
int beat_col = 0;
Button lowBtn;
Button highBtn;
Button beatBtn;
Button applyBtn;

RadioButton racks;

float sliderR = 0;
float sliderG = 0;
float sliderB = 0;

OscP5 oscP5;
NetAddress portIn;
boolean continuosMode = false;

void setup() 
{
  frameRate(framerate);
  bg = loadImage("bg.jpg");
  size(W,H);
  smooth();
  
  // Init OSC Plugs
  initOsc();
 
  //Minim init
  minim = new Minim(this);
  beat = new BeatDetect();
  
  //minim.debugOn();
  in = minim.getLineIn(Minim.STEREO,512);
  fft = new FFT(in.bufferSize(), in.sampleRate());
  
  //Open serial communication
  if (!offline){
    String portName = Serial.list()[1];
    myPort = new Serial(this, portName, 115200);
  }
  
  controlP5 = new ControlP5(this);
  //controlP5.setControlFont(new ControlFont(createFont("Georgia",20), 20));
 
  // COLOR SETTER AREA
  knobR = controlP5.addKnob("knobR",0,1023,0,20,20,70);
  knobR.showTickMarks(true);
  lblKnobR = controlP5.addTextlabel("lblKnobR","RED",40,100);
  //resetR_btn = controlP5.addButton("ResetR",1,20,110,40,20);
  controlP5.controller("knobR").addListener(knobListener);
  
  knobG = controlP5.addKnob("knobG",0,1023,0,115,20,70);
  knobG.showTickMarks(true);
  lblKnobG = controlP5.addTextlabel("lblKnobG","GREEN",135,100);
  //resetG_btn = controlP5.addButton("ResetG",1,80,110,40,20);
  controlP5.controller("knobG").addListener(knobListener);
  
  knobB = controlP5.addKnob("knobB",0,1023,0,210,20,70);
  knobB.showTickMarks(true);
  lblKnobB = controlP5.addTextlabel("lblKnobB","BLUE",235,100);
  //resetB_btn = controlP5.addButton("ResetB",1,140,110,40,20);
  controlP5.controller("knobB").addListener(knobListener);
  
  // ALL TRIGGER AREA
  allTrigger0_btn = controlP5.addButton("All0",1,305,120,64,40);
  allTrigger1_btn = controlP5.addButton("All1",1,375,120,64,40);
  allTrigger2_btn = controlP5.addButton("All2",1,445,120,64,40);
  allTrigger3_btn = controlP5.addButton("All3",1,515,120,64,40);
  
  //AUDIO SYNC RELATED
  highBtn = controlP5.addButton("High",1,20,240,64,54);
  lowBtn = controlP5.addButton("Low",1,90,240,64,54);
  beatBtn = controlP5.addButton("Beat",1,160,240,64,54);
  applyBtn = controlP5.addButton("Apply",1,230,240,48,54);
  applyBtn.setSwitch(true);
  
  // COL/ROW AREA
  knobRow = controlP5.addKnob("knobRow",0,nRows-1,0,310,180,50);
  knobRow.showTickMarks(true);
  knobRow_btn = controlP5.addButton("Row",1,304,240,64,20);
  knobRow_lbl = controlP5.addTextlabel("knobRow_lbl","0",350,246);
  
  knobCol = controlP5.addKnob("knobCol",0,nCols-1,0,380,180,50);
  knobCol.showTickMarks(true);
  knobCol_btn = controlP5.addButton("Col",1,374,240,64,20);
  knobCol_lbl = controlP5.addTextlabel("knobCol_lbl","0",420,246);
  
  // DIAGONAL AREA
  knobDia1 = controlP5.addKnob("knobDia1",0,nRows*2-2,0,450,180,50);
  knobDia1.showTickMarks(true);
  knobDia1_btn = controlP5.addButton("Dia1",1,444,240,64,20);
  knobDia1_lbl = controlP5.addTextlabel("knobDia1_lbl","0",490,246);
  
  knobDia2 = controlP5.addKnob("knobDia2",0,nCols*2-2,0,520,180,50);
  knobDia2.showTickMarks(true);
  knobDia2_btn = controlP5.addButton("Dia2",1,514,240,64,20);
  knobDia2_lbl = controlP5.addTextlabel("knobDia2_lbl","0",570,246);
  
  // MATRIX SINGLES
   //Initialize slots
  //ControlGroup pads = new ControlGroup()
  for (int j=0;j<nCols;j++){
    for (int i=0;i<nRows;i++){
     if(j%2==1) matrixPads[j][i] = controlP5.addButton("pad"+((j*nRows)+i),(j*nRows)+i,305+(i*70),270+(j*70),64,64);
     else matrixPads[j][i] = controlP5.addButton("pad"+((j*nRows)+i),(j*nRows)+i,305+(nRows*70)-((i+1)*70),270+(j*70),64,64); 
     controlP5.controller("pad"+((j*nRows)+i)).addListener(padListener);
    }  
  }
  
  saveBtn = controlP5.addButton("saveF",0,16,490,58,54);
  loadBtn = controlP5.addButton("loadF",0,80,490,64,54);
  resetItemsBtn = controlP5.addButton("reset",0,150,490,64,54);
  stealthModeBtn = controlP5.addButton("stealth",0,220,490,60,54);
  stealthModeBtn.setSwitch(true);
  
  //Initialize slots
  for (int j=0;j<nRacks;j++){
    for (int i=0;i<nSlots;i++){
     slots[j][i] = new Slot(0,HEAD_ALL,null);
    }  
  }
  
  //Pitch control
  controlP5.addSlider("pitch",60,600,300,slotPadd,H - 2*slotPadd - slotHeight -40,100,40);
  playBtn = controlP5.addButton("play",0,150,H - 2*slotPadd - slotHeight -40,40,40);
  stopBtn = controlP5.addButton("stop",0,200,H - 2*slotPadd - slotHeight -40,40,40);
  audioSyncBtn = controlP5.addButton("sync",0,250,H - 2*slotPadd - slotHeight -40,40,40);
  audioSyncBtn.setSwitch(true);
  loopBtn = controlP5.addButton("looping",0,300,H - 2*slotPadd - slotHeight -40,40,40);
  loopBtn.setSwitch(true);
  
  //racks
  racks = controlP5.addRadioButton("racks",350,H - 2*slotPadd - slotHeight -40);
  racks.setColorForeground(color(120));
  racks.setColorActive(color(255));
  racks.setColorLabel(color(255));
  racks.setItemsPerRow(nRacks);
  racks.setSpacingColumn(12);
  racks.setItemWidth(10);
  racks.setItemHeight(40);
  
  //Add racks
  for (int i=0;i<nRacks;i++){
    addToRadioButton(racks,Integer.toString(i),i);
  }
  
  racks.activate(0);
  
  activateRackBtn = controlP5.addButton("setRack",0,530,H - 2*slotPadd - slotHeight -40,50,40);
 
}

void draw() {
  background(bg);
  
  stroke(255); 
  
  //Draw color indicator PICKER
  fill(color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB)));
  rect(20,120,256,110);
  
  // Audio snc area
  fft.forward(in.mix);
  beat.detect(in.mix);
  if (beat.isOnset()) stroke(255,0,0);
  else stroke(255);
  fill(122,122,122);
  rect(20,300,fft.specSize(),180);
  for (int i = 0; i < fft.specSize(); i++)
  {
     // draw the line for frequency band i, 
     // scaling it by 4 so we can see it a bit better
     line(20+i, 480, 20+i, 480 - fft.getBand(i));
  } 
  float lowA = fft.getFreq(400); //400Hz    
  float highA = fft.getFreq(600); //2000Hz
  
  //audio sync stuff
  if (audioApply){
    
    //high vs low
    if (lowA > 3 && lowA> highA){
      println("LOW = "+lowA);
      if (low_col != 0) transferAllChannelsColor(low_col); 
    }else if(highA > 3 && highA> lowA){
      println("HIGH = "+highA);
      if (high_col != 0) transferAllChannelsColor(high_col); 
    }
    
    //beat
    if (beat.isOnset()){
      transferAllChannelsColor(beat_col); 
    }
  }
  
  
  //Draw slots
  for (int i=0;i<nSlots;i++){
    //Draw slots
    fill(slots[currentRack][i].col);
    if (i == currentSlot) strokeWeight(4);    
    if (i == activeSlot) stroke(255,0,0);
    rect(slotPadd + i*(slotPadd + slotWidth),H - slotPadd - slotHeight,slotWidth,slotHeight);
    strokeWeight(1);
    stroke(255);      
  }
  
  //if playing than loop
  if (playing){
    beatCounter++;
    //println(beatCounter+"/"+(framerate / (pitch/framerate)));
    if (beatCounter >= (framerate / (pitch/framerate))){
      activeSlot = ((activeSlot+1) % (nSlots));
      beatCounter = 0;
      slots[activeRack][activeSlot].trigger();
      
      //println("activeSlot = "+activeSlot +"activeRack = "+activeRack );
      
      if (activeSlot == 0 && looping && !activeRacks[((activeRack+1) % (nRacks))]){
        activeRack = 0; 
        racks.activate(activeRack);
      }else{
        if (activeSlot == 0 && looping && activeRacks[((activeRack+1) % (nRacks))]){
          activeRack = ((activeRack+1) % (nRacks));
          racks.activate(activeRack);
        } 
      }
    }  
  }
  
  if (playing && audioSync && beat.isOnset()){
    activeSlot = ((activeSlot+1) % (nSlots));
    beatCounter = 0;
    slots[activeRack][activeSlot].trigger();
  }
  
}

// P5CONTROL CALLBACK FUNCTIONS
// --------------------------------------


// COLOR PICKER
public void ResetR(int theValue) {
  knobR(0);
}

public void ResetG(int theValue) {
  knobG(0);
}

public void ResetB(int theValue) {
  knobB(0);
}

void knobR(float r) {
  sliderR = r;
  //knobR.setValue(r);
}

void knobG(float g) {
  sliderG = g;
  //knobG.setValue(g);
}

void knobB(float b) {
  sliderB = b;
  //knobB.setValue(b);
}

void pitch(float value) {
  pitch = (int)value;
}

void play(int theValue){
  playing = true;
}

void looping(int theValue){
  looping = !loopBtn.booleanValue();
}

void stop(int theValue){
  playing = false;
}

void sync(int theValue){
  audioSync = !audioSyncBtn.booleanValue();
  playing = true;
}

void Apply(int theValue){
  audioApply = !applyBtn.booleanValue();
}

void addToRadioButton(RadioButton theRadioButton, String theName, int theValue ) {
  RadioButton t = theRadioButton.addItem(theName,theValue);
  t.captionLabel().setColorBackground(color(80));
  t.captionLabel().style().movePadding(2,0,-1,2);
  t.captionLabel().style().moveMargin(-2,0,0,-3);
  t.captionLabel().style().backgroundWidth = 10;
  t.captionLabel().style().backgroundHeight = 20;
}

void setRack(int theValue){
  activeRack = currentRack;
}

void reset(int theValue){
  //ControlGroup pads = new ControlGroup()
  for (int j=0;j<nCols;j++){
    for (int i=0;i<nRows;i++){
      matrixPads[j][i].setColorBackground(0);
      matrix[j*nCols+i] = 0;
    }  
  }
}

void stealth(int theValue){
 stealth=!stealth;
}

void controlEvent(ControlEvent theEvent) {
  if (theEvent.group()!=null){
     if (theEvent.group().name().equals("racks")){
        currentRack = (int)theEvent.group().value();
     }
     //println(theEvent.id());
  }
  
}

// ALL SETTERS
public void All0(int theValue) {
  if(keyPressed) {
    if (checkKey(KeyEvent.VK_SPACE)) {
      allTrigger0_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      allTrigger0_btn.setColorBackground(allTrigger0_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      slots[currentRack][currentSlot] = new Slot(allTrigger0_col,HEAD_ALL,null);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }
  }else{
    //transferAllChannelsColor(0);    
    transferAllChannelsColor(allTrigger0_col);    
  }
}

public void All1(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      allTrigger1_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      allTrigger1_btn.setColorBackground(allTrigger1_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      slots[currentRack][currentSlot] = new Slot(allTrigger1_col,HEAD_ALL,null);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }
  }else{
    //transferAllChannelsColor(0);
    transferAllChannelsColor(allTrigger1_col);
  }
}

public void All2(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      allTrigger2_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      allTrigger2_btn.setColorBackground(allTrigger2_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      slots[currentRack][currentSlot] = new Slot(allTrigger2_col,HEAD_ALL,null);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }
  }else{
    //transferAllChannelsColor(0);
    transferAllChannelsColor(allTrigger2_col);    
  }
}

public void All3(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      allTrigger3_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      allTrigger3_btn.setColorBackground(allTrigger3_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      slots[currentRack][currentSlot] = new Slot(allTrigger3_col,HEAD_ALL,null);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }
  }else{
    transferAllChannelsColor(0);
    transferAllChannelsColor(allTrigger3_col);    
  }
}

//AUDIO SYNC RELATED
public void High(int theValue) {
  if(keyPressed) {
    if (checkKey(KeyEvent.VK_SPACE)) {
      high_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      highBtn.setColorBackground(high_col);
    }else if (checkKey(KeyEvent.VK_R)) {
      highBtn.setColorBackground(0);
      high_col = 0;
    }
  }
}

public void Low(int theValue) {
  if(keyPressed) {
    if (checkKey(KeyEvent.VK_SPACE)) {
      low_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      lowBtn.setColorBackground(low_col);
    }else if (checkKey(KeyEvent.VK_R)) {
      lowBtn.setColorBackground(0);
      low_col = 0;
    }
  }
}

public void Beat(int theValue) {
  if(keyPressed) {
    if (checkKey(KeyEvent.VK_SPACE)) {
      beat_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      beatBtn.setColorBackground(beat_col);
    }else if (checkKey(KeyEvent.VK_R)) {
      beatBtn.setColorBackground(0);
      beat_col = 0;
    }
  }
}

// ROW/COL AREA
void knobRow(float r) {
  row = (int)r;
  knobRow_lbl.setValue(Integer.toString(row));
  transferRowColor(row,knobRow_col);
}

void knobCol(float c) {
  col = (int)c;
  knobCol_lbl.setValue(Integer.toString(col));
  transferColColor(col,knobCol_col);
}

public void Row(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      knobRow_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      knobRow_btn.setColorBackground(knobRow_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      ArrayList params = new ArrayList();
      params.add(row);
      slots[currentRack][currentSlot] = new Slot(knobRow_col,HEAD_ROW,params);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }else if (checkKey(KeyEvent.VK_R)) {
      knobRow_col = 0;
      knobRow_btn.setColorBackground(0);
    }
  }else{
    //transferAllChannelsColor(0);
    transferRowColor(row,knobRow_col);
  }
}

public void Col(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      knobCol_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      knobCol_btn.setColorBackground(knobCol_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      ArrayList params = new ArrayList();
      params.add(col);
      slots[currentRack][currentSlot] = new Slot(knobCol_col,HEAD_COL,params);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }else if (checkKey(KeyEvent.VK_R)) {
      knobCol_col = 0;
      knobCol_btn.setColorBackground(0);
    }
  }else{
    //transferAllChannelsColor(0);
    transferColColor(col,knobCol_col);
  }
}


// DIAGONAL AREA
void knobDia1(float r) {
  dia1 = (int)r;
  knobDia1_lbl.setValue(Integer.toString(dia1));
  transferDia1Color(dia1,dia1_col);
}

void knobDia2(float c) {
  dia2 = (int)c;
  knobDia2_lbl.setValue(Integer.toString(dia2));
  transferDia2Color(dia2,dia2_col);
}

public void Dia1(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      dia1_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      knobDia1_btn.setColorBackground(dia1_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      ArrayList params = new ArrayList();
      params.add(dia1);
      slots[currentRack][currentSlot] = new Slot(dia1_col,HEAD_DIA1,params);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }else if (checkKey(KeyEvent.VK_R)) {
      dia1_col = 0;
      knobDia1_btn.setColorBackground(0);
    }
  }else{
    transferAllChannelsColor(0);
    //Transfer Row
  }
}

public void Dia2(int theValue) {
  if(keyPressed) {
    if (key == ' ') {
      dia2_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
      knobDia2_btn.setColorBackground(dia2_col);
    }else if (checkKey(KeyEvent.VK_S)) {
      ArrayList params = new ArrayList();
      params.add(dia2);
      slots[currentRack][currentSlot] = new Slot(dia2_col,HEAD_DIA2,params);
      currentSlot = ((currentSlot+1) % (nSlots));
      activeRacks[currentRack] = true;
    }else if (checkKey(KeyEvent.VK_R)) {
      dia2_col = 0;
      knobDia2_btn.setColorBackground(0);
    }
  }else{
    transferAllChannelsColor(0);
    //Transfer Row
  }
}

int sb2rgb(float v){
  float d = v/1023;
  return int(255*d);
}

// FUNCTIONS TO TRANSFER VALUES TO MATRIX
// --------------------------------------

void transferColor(int head){
  if (offline) return;
  myPort.write(head);
  if (head==HEAD_R){
     myPort.write(sb2rgb(sliderR)); 
  }else if (head==HEAD_G){
     myPort.write(sb2rgb(sliderG));    
  }else if (head==HEAD_B){
     myPort.write(sb2rgb(sliderB));    
  }
  println("R "+sb2rgb(sliderR)+" G "+sb2rgb(sliderG)+" B "+sb2rgb(sliderB));
}

// Transfer a color 
void transferAllChannelsColor(int colorToTransfer){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(sb2rgb( (colorToTransfer >> 16) & 0xFF )); 
  myPort.write(HEAD_G);
  myPort.write(sb2rgb( (colorToTransfer >> 8) & 0xFF )); 
  myPort.write(HEAD_B);
  myPort.write(sb2rgb( (colorToTransfer) & 0xFF )); 
  myPort.write(HEAD_ALL);
}

// Transfer a row 
void transferRowColor(int row,int colorToTransfer){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(sb2rgb( (colorToTransfer >> 16) & 0xFF )); 
  myPort.write(HEAD_G);
  myPort.write(sb2rgb( (colorToTransfer >> 8) & 0xFF )); 
  myPort.write(HEAD_B);
  myPort.write(sb2rgb( (colorToTransfer) & 0xFF )); 
  myPort.write(HEAD_ROW);
  myPort.write(row); 
}

// Transfer a col 
void transferColColor(int col,int colorToTransfer){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(sb2rgb( (colorToTransfer >> 16) & 0xFF )); 
  myPort.write(HEAD_G);
  myPort.write(sb2rgb( (colorToTransfer >> 8) & 0xFF )); 
  myPort.write(HEAD_B);
  myPort.write(sb2rgb( (colorToTransfer) & 0xFF )); 
  myPort.write(HEAD_COL);
  myPort.write(col); 
}

// Transfer a col 
void transferDia1Color(int dia1,int colorToTransfer){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(sb2rgb( (colorToTransfer >> 16) & 0xFF )); 
  myPort.write(HEAD_G);
  myPort.write(sb2rgb( (colorToTransfer >> 8) & 0xFF )); 
  myPort.write(HEAD_B);
  myPort.write(sb2rgb( (colorToTransfer) & 0xFF )); 
  myPort.write(HEAD_DIA1);
  myPort.write(dia1); 
}

// Transfer a col 
void transferDia2Color(int dia2,int colorToTransfer){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(sb2rgb( (colorToTransfer >> 16) & 0xFF )); 
  myPort.write(HEAD_G);
  myPort.write(sb2rgb( (colorToTransfer >> 8) & 0xFF )); 
  myPort.write(HEAD_B);
  myPort.write(sb2rgb( (colorToTransfer) & 0xFF )); 
  myPort.write(HEAD_DIA2);
  myPort.write(dia2); 
}

// Transfer the matrix
void transferMatrix(){
  if (offline) return;
  //RED
  myPort.write(HEAD_R);
  myPort.write(0); 
  myPort.write(HEAD_G);
  myPort.write(0); 
  myPort.write(HEAD_B);
  myPort.write(0); 
  myPort.write(HEAD_MATRIX);
  for (int i=0;i<matrix.length;i++){
    int col = matrix[i];
    myPort.write(sb2rgb( (col >> 16) & 0xFF )); 
    myPort.write(sb2rgb( (col >> 8) & 0xFF ));
    myPort.write(sb2rgb( col & 0xFF )); 
  }
  
}

// KEY HANDLING

boolean checkKey(int k)
{
  if (keys.length >= k) {
    return keys[k];  
  }
  return false;
}

void keyPressed()
{ 
  keys[keyCode] = true;
  println(KeyEvent.getKeyText(keyCode));
  if (checkKey(KeyEvent.VK_RIGHT) && currentSlot < nSlots) currentSlot++;
  else if (checkKey(KeyEvent.VK_LEFT) && currentSlot > 0) currentSlot--;
  else if (checkKey(KeyEvent.VK_UP) && currentRack < nRacks-1){
     currentRack++;
     racks.activate(currentRack);
  }
  else if (checkKey(KeyEvent.VK_DOWN) && currentRack > 0){
    currentRack--;
    racks.activate(currentRack);
  }/*else if (checkKey(KeyEvent.VK_G)){
    saveF();
  }else if (checkKey(KeyEvent.VK_H)){
    loadF();
  }*/
  
  //if we are in creationMode, autosave
  if(creatingMode){
    storeRackInfo("temp.xml"); 
  }
  
}
 
void keyReleased()
{ 
  keys[keyCode] = false; 
}

// UTILITY METHODS
int fact(int n) {
 if (n <= 1) {
   return 1;
 }
 else {
   return n * fact(n-1);
 }
} 

//Store rack information to XML

void storeRackInfo(String file){
  
  XMLElement toStore= new XMLElement();
  toStore.setName("racks");
  toStore.setString( "nCols",Integer.toString(nCols));
  toStore.setString( "nRows",Integer.toString(nRows));  
  //toStore.setContent("inhalt");
  
  for (int i=0;i<nRacks;i++){
    
    XMLElement rack = new XMLElement();
    rack.setName("rack");
    rack.setString( "id",Integer.toString(i));
    
    for (int j=0;j<nSlots;j++){
      
      Slot s = slots[i][j];
      
      XMLElement item = new XMLElement();
      item.setName("item");
      item.setString( "col",Integer.toString(s.col));
      item.setString( "type",Integer.toString(s.type));
      
      String params = "";
      if (s.params!=null){
        for (int k=0;k<s.params.size();k++){
          params += Integer.toString((Integer)s.params.get(k));
          params += ",";
          item.setString( "params",params);
        }  
      }
      
      rack.addChild(item);
    
    }
    
    toStore.addChild(rack);
    
  }
  
  PrintWriter xmlfile;
  xmlfile = createWriter(file);
  
  //xml schreiben
  try
  {
    XMLWriter schreibXML = new XMLWriter(xmlfile) ;
  
    schreibXML.write(toStore);
  
    xmlfile.flush();
    xmlfile.close();
  }
  catch (IOException e)
  {
    e.printStackTrace();
  } 
}

void loadRackInfo(String file){
  
  XMLElement toLoad= new XMLElement(this,file);
  int nCols = toLoad.getIntAttribute("nCols");
  int nRows = toLoad.getIntAttribute("nRows");

  XMLElement[] racks = toLoad.getChildren();
  
  for (int i=0;i<racks.length;i++){
    
    XMLElement rack = racks[i];
    int id = rack.getIntAttribute("id");
    
    XMLElement[] items = rack.getChildren();
    
    for (int j=0;j<items.length;j++){
      
      XMLElement item = items[j];
      
      int col = item.getIntAttribute("col");
      int type = item.getIntAttribute("type");
      ArrayList params = new ArrayList();   
      String pAttr = item.getStringAttribute("params");
      
      if (pAttr!=null){
        String[] l = split(pAttr, ',');
      
        for (int k=0;k<l.length;k++){
          params.add(int(l[k]));       
        }  
      }    
    
      if (col!=0){
        activeRacks[i] = true;
      }  
      
      slots[i][j] = new Slot(col,type,params);    
    }
  }
}

void saveF(){
  String savePath = selectOutput();  // Opens file chooser
  if (savePath == null) {
    // If a file was not selected
    println("No output file was selected...");
  } else {
    // If a file was selected, print path to folder
    println(savePath);
    storeRackInfo(savePath);
  }
}

void loadF(){
  String loadPath = selectInput();  // Opens file chooser
  if (loadPath == null) {
    // If a file was not selected
    println("No file was selected...");
  } else {
    // If a file was selected, print path to file
    println(loadPath);
    loadRackInfo(loadPath);
  }
}

class PadListener implements ControlListener {
  int index;
  public void controlEvent(ControlEvent theEvent) {
    println("i got an event from mySlider, " +
            "changing background color to "+
            theEvent.controller().value());
    index = (int)theEvent.controller().value();
    if(keyPressed) {
      if (key == ' ') {
        matrix[index] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
        theEvent.controller().setColorBackground(matrix[index]);
        if (!stealth) transferMatrix();    
      }else if (checkKey(KeyEvent.VK_S)) {
        ArrayList params = new ArrayList(); 
        for (int i=0;i<matrix.length;i++){
          params.add(matrix[i]);
        }
        slots[currentRack][currentSlot] = new Slot(matrix[index],HEAD_MATRIX,params);
        currentSlot = ((currentSlot+1) % (nSlots));
      }else if (checkKey(KeyEvent.VK_R)) {
        matrix[index] = 0;
        theEvent.controller().setColorBackground(color(004762));
        if (!stealth) transferMatrix();    
      }
    }
    else{
      transferMatrix();    
    }
  }
}

class KnobListener implements ControlListener {
  int index;
  public void controlEvent(ControlEvent theEvent) {
    if(keyPressed) {
      if (checkKey(KeyEvent.VK_R)) {
        if (theEvent.controller().name().equals("knobR")){
           println("knobR");
           //knobR.setValue(0);
           sliderR = 0;
        }else if (theEvent.controller().name().equals("knobG")){
           //knobG.setValue(0);
           sliderG = 0;
        }else if (theEvent.controller().name().equals("knobB")){
           //knobB.setValue(0);
           sliderB = 0;
        }
      }
    }
  }
}

/*************************************************
 OSC
 *************************************************/

void initOsc() {  

  // start oscP5, listening for incoming messages at port 12000
  oscP5 = new OscP5(this, 12000);
  portIn = new NetAddress("127.0.0.1", 12000);

  //Define plugs
  oscP5.plug(this, "mainROsc", "/1/r");
  oscP5.plug(this, "mainGOsc", "/1/g");
  oscP5.plug(this, "mainBOsc", "/1/b");
  oscP5.plug(this, "continuosModeOsc", "/1/continuosMode");
  oscP5.plug(this, "highOsc", "/1/high");
  oscP5.plug(this, "lowOsc", "/1/low");
  oscP5.plug(this, "beatOsc", "/1/beat");
  oscP5.plug(this, "applyOsc", "/1/apply");
  oscP5.plug(this, "rowOsc", "/1/row");
  oscP5.plug(this, "colOsc", "/1/col");
  oscP5.plug(this, "diagonal1Osc", "/1/diagonal1");
  oscP5.plug(this, "diagonal2Osc", "/1/diagonal2");
  oscP5.plug(this, "pitchOsc", "/1/pitch");
  oscP5.plug(this, "playOsc", "/1/play");
  oscP5.plug(this, "stopOsc", "/1/stop");
  oscP5.plug(this, "syncOsc", "/1/sync");
  oscP5.plug(this, "loopingOsc", "/1/looping");
  oscP5.plug(this, "cell0", "/1/cell/3/1");
  oscP5.plug(this, "cell1", "/1/cell/3/2");
  oscP5.plug(this, "cell2", "/1/cell/3/3");
  oscP5.plug(this, "cell3", "/1/cell/3/4");
  oscP5.plug(this, "cell4", "/1/cell/2/1");
  oscP5.plug(this, "cell5", "/1/cell/2/2");
  oscP5.plug(this, "cell6", "/1/cell/2/3");
  oscP5.plug(this, "cell7", "/1/cell/2/4");
  oscP5.plug(this, "cell8", "/1/cell/1/1");
  oscP5.plug(this, "cell9", "/1/cell/1/2");
  oscP5.plug(this, "cell10", "/1/cell/1/3");
  oscP5.plug(this, "cell11", "/1/cell/3/4");
  oscP5.plug(this, "cell12", "/1/cell/4/1");
  oscP5.plug(this, "cell13", "/1/cell/4/2");
  oscP5.plug(this, "cell14", "/1/cell/4/3");
  oscP5.plug(this, "cell15", "/1/cell/4/4");
  oscP5.plug(this, "resetMatrixOsc", "/1/resetMatrix");
  
}

public void resetMatrixOsc(float value) {
  if (DEBUG) println("resetMatrixOsc: "+value);
  reset(0);
}

public void cell0(float value) {
  if (DEBUG) println("cell0: "+value);
  matrix[0] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell1(float value) {
  if (DEBUG) println("cell1: "+value);
  matrix[1] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell2(float value) {
  if (DEBUG) println("cell2: "+value);
  matrix[2] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell3(float value) {
  if (DEBUG) println("cell3: "+value);
  matrix[3] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell4(float value) {
  if (DEBUG) println("cell4: "+value);
  matrix[4] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell5(float value) {
  if (DEBUG) println("cell5: "+value);
  matrix[5] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell6(float value) {
  if (DEBUG) println("cell6: "+value);
  matrix[6] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell7(float value) {
  if (DEBUG) println("cell7: "+value);
  matrix[7] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell8(float value) {
  if (DEBUG) println("cell8: "+value);
  matrix[8] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell9(float value) {
  if (DEBUG) println("cell9: "+value);
  matrix[9] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell10(float value) {
  if (DEBUG) println("cell10: "+value);
  matrix[10] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell11(float value) {
  if (DEBUG) println("cell11: "+value);
  matrix[11] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell12(float value) {
  if (DEBUG) println("cell12: "+value);
  matrix[12] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell13(float value) {
  if (DEBUG) println("cell13: "+value);
  matrix[13] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell14(float value) {
  if (DEBUG) println("cell14: "+value);
  matrix[14] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void cell15(float value) {
  if (DEBUG) println("cell15: "+value);
  matrix[15] = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  transferMatrix();
}

public void playOsc(float value) {
  if (DEBUG) println("playOsc: "+value);
     playing = true;
}

public void stopOsc(float value) {
  if (DEBUG) println("stopOsc: "+value);
     playing = false;
}

public void syncOsc(float value) {
  if (DEBUG) println("syncOsc: "+value);
  if (value == 1)
     audioSync = true;
  else
     audioSync = false;
}

public void loopingOsc(float value) {
  if (DEBUG) println("loopingOsc: "+value);
  if (value == 1)
     looping = true;
  else
     looping = false;
}

public void pitchOsc(float value) {
  if (DEBUG) println("pitch: "+value);
  pitch = (int)value;
}

public void continuosModeOsc(float value) {
  if (DEBUG) println("continuosMode: "+value);
  continuosMode = !continuosMode;
}

public void rowOsc(float value) {
  if (DEBUG) println("row: "+(int)value);
  knobRow_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  knobRow((int)value);
}

public void colOsc(float value) {
  if (DEBUG) println("col: "+(int)value);
  knobCol_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  knobCol((int)value);
}

public void diagonal1Osc(float value) {
  if (DEBUG) println("diagonal1: "+(int)value);
  dia1_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  knobDia1((int)value);
}

public void diagonal2Osc(float value) {
  if (DEBUG) println("diagonal2: "+(int)value);
  dia2_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
  knobDia2((int)value);
}

public void highOsc(float value) {
  if (DEBUG) println("high: "+value);
  if (value==1){
    high_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
    highBtn.setColorBackground(high_col);  
  }else{
    high_col = color(0);
    highBtn.setColorBackground(0);  
  }
}

public void lowOsc(float value) {
  if (DEBUG) println("low: "+value);
  if (value==1){
    low_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
    lowBtn.setColorBackground(high_col);  
  }else{
    low_col = color(0);
    lowBtn.setColorBackground(0);  
  }  
}

public void beatOsc(float value) {
  if (DEBUG) println("beat: "+value);
  if (value==1){
    beat_col = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB));
    beatBtn.setColorBackground(high_col);  
  }else{
    beat_col = color(0);
    beatBtn.setColorBackground(0);  
  }
}

public void applyOsc(float value) {
  if (DEBUG) println("apply: "+value);
  if (value==1){
    applyBtn.setSwitch(true);
    audioApply = true;
  }else{
    applyBtn.setSwitch(false);
    audioApply = false;  
  }
}

public void mainROsc(float value) {
  if (DEBUG) println("mainR: "+value);
  sliderR = (int)value;
  knobR(sliderR);
  if (continuosMode){
    color c = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB)); 
    transferAllChannelsColor(c);
  } 
}

public void mainGOsc(float value) {
  if (DEBUG) println("mainG: "+value);
  sliderG = (int)value;
  knobG(sliderG);
  if (continuosMode){
    color c = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB)); 
    transferAllChannelsColor(c);
  } 
}

public void mainBOsc(float value) {
  if (DEBUG) println("mainB: "+value);
  sliderB = (int)value;
  knobB(sliderB);
  if (continuosMode){
    color c = color(sb2rgb(sliderR),sb2rgb(sliderG),sb2rgb(sliderB)); 
    transferAllChannelsColor(c);
  } 
}



